class TennisScoreboard:
    def _init_(self):
        self.player1_points = 0
        self.player2_points = 0
        self.game_state = "in_progress"
        self.score_map = [0, 15, 30, 40]

    def update_score(self, score_string):
        for point in score_string:
            if point == "A":
                self.player1_points += 1
            elif point == "B":
                self.player2_points += 1
        self.check_game_state()

    def check_game_state(self):
        if self.player1_points >= 4 and self.player1_points - self.player2_points >= 2:
            self.game_state = "Player A wins"
        elif self.player2_points >= 4 and self.player2_points - self.player1_points >= 2:
            self.game_state = "Player B wins"
        elif self.player1_points >= 3 and self.player2_points >= 3:
            if self.player1_points == self.player2_points:
                self.game_state = "deuce"
            elif self.player1_points == self.player2_points + 1:
                self.game_state = "advantage Player A"
            elif self.player2_points == self.player1_points + 1:
                self.game_state = "advantage Player B"
        else:
            self.game_state = "in_progress"

    def get_score(self):
        if self.game_state == "in_progress":
            player1_score = self.score_map[min(self.player1_points, 3)]
            player2_score = self.score_map[min(self.player2_points, 3)]
            return f"Player A: {player1_score}, Player B: {player2_score}"
        else:
            return self.game_state

scoreboard = TennisScoreboard()
scoreboard.update_score("AAB")
print(scoreboard.get_score())
